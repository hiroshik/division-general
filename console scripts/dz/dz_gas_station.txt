script.begin name="dz_gas_station_a"

rogue.waitForInGame

script.wait time="1"
console.cmd cmd="Rogue.SetLevel 12"

script.wait time="1"
console.cmd cmd="Rogue.Item.SpawnInInventory player_back_generated_item Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_chest_generated_item Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_hands_generated_item Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_knees_generated_item Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_thighs_generated_item Green"

script.wait time="1"
console.cmd cmd="Rogue.EquipItem player_back_generated_item"
console.cmd cmd="Rogue.EquipItem player_chest_generated_item"
console.cmd cmd="Rogue.EquipItem player_hands_generated_item"
console.cmd cmd="Rogue.EquipItem player_knees_generated_item"
console.cmd cmd="Rogue.EquipItem player_thighs_generated_item"

script.wait time="1"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_assaultrifle_ACR_t1_v1 Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_marksmanrifle_m1a_t1_v1 Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_pistol_m9_t1_v1 Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_lightmachinegun_m249_t1_v1 Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_shotgun_870_t1_v1 Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_pistol_m9_t1_v1 Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_submachinegun_821_t1_v1 Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_marksmanrifle_m1a_t1_v1 Green"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_pistol_m9_t1_v1 Green"

script.wait time="1"
console.cmd cmd="Rogue.SetWeaponInSlot player_weapon_assaultrifle_ACR_t1_v1 0"
console.cmd cmd="Rogue.SetWeaponInSlot player_weapon_marksmanrifle_m1a_t1_v1 1"
console.cmd cmd="Rogue.SetWeaponInSlot player_weapon_pistol_m9_t1_v1 2"

script.wait time="1"
console.cmd cmd="Rogue.EquipItem player_weapon_assaultrifle_ACR_t1_v1"

script.wait time="1"
console.cmd cmd="Rogue.UnlockFastTravel"

script.wait time="1"
console.cmd cmd="Rogue.SetPosition 177.3,0.13,591.3"

script.wait time="1"
console.cmd cmd="Rogue.SetDarkZoneRank 3"

script.end