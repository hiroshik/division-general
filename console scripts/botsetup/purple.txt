script.begin name="t3_p2_new.txt"
rogue.waitForInGame
console.cmd cmd="Rogue.SetLevel 12.5"

console.cmd cmd="Rogue.E32015.SetTeam 3"
console.cmd cmd="Rogue.E32015.SetMember 2"

script.wait time="1"
console.cmd cmd="Rogue.DamageSelf -20000"

script.wait time="1"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_face_player_megan Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_chest_player_megan Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_back_player_megan Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_hands_player_megan Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_thighs_player_megan Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_knees_player_megan Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_hat_player_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_scarf_player_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_shirt_player_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_jacket_player_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pants_player_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_shoes_player_megan Green"

script.wait time="2"
console.cmd cmd="Rogue.EquipItem brand_face_player_megan"
console.cmd cmd="Rogue.EquipItem brand_chest_player_megan"
console.cmd cmd="Rogue.EquipItem brand_back_player_megan"
console.cmd cmd="Rogue.EquipItem brand_hands_player_megan"
console.cmd cmd="Rogue.EquipItem brand_thighs_player_megan"
console.cmd cmd="Rogue.EquipItem brand_knees_player_megan"
console.cmd cmd="Rogue.EquipItem brand_hat_player_megan"
console.cmd cmd="Rogue.EquipItem brand_scarf_player_megan"
console.cmd cmd="Rogue.EquipItem brand_shirt_player_megan"
console.cmd cmd="Rogue.EquipItem brand_jacket_player_megan"
console.cmd cmd="Rogue.EquipItem brand_pants_player_megan"
console.cmd cmd="Rogue.EquipItem brand_shoes_player_megan"

console.cmd cmd="Rogue.Item.SpawnInInventory Player_weapon_submachinegun_tec9_t1_v1 Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory Player_weapon_marksmanrifle_m1a_t2_v1 Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory Player_weapon_pistol_m&p45_t1_v1 Blue"

script.wait time="1"
console.cmd cmd="Rogue.EquipItem Player_weapon_pistol_m&p45_t1_v1"
script.wait time="1"
console.cmd cmd="Rogue.EquipItem Player_weapon_marksmanrifle_m1a_t2_v1"
script.wait time="1"
console.cmd cmd="Rogue.EquipItem Player_weapon_submachinegun_tec9_t1_v1"

script.wait time="1"
console.cmd cmd="Rogue.SetWeaponInSlot Player_weapon_submachinegun_tec9_t1_v1 0"
console.cmd cmd="Rogue.SetWeaponInSlot Player_weapon_marksmanrifle_m1a_t2_v1 1"
console.cmd cmd="Rogue.SetWeaponInSlot Player_weapon_pistol_m&p45_t1_v1 2"

script.wait time="1"
console.cmd cmd="Rogue.Skill.AssignToLB player_sticky_bomb"
console.cmd cmd="Rogue.Skill.AssignToRB player_seeker_mine"
console.cmd cmd="Rogue.SignatureSkill.SetDefensive"
console.cmd cmd="Rogue.Talent.FillSlotsWithRandom"

script.end