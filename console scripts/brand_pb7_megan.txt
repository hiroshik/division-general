script.begin name="brand_pb7_megan"
rogue.waitForInGame

script.wait time="1"
console.cmd cmd="Rogue.SetLevel 3"

script.wait time="2"

console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_face_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_chest_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_back_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_hands_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_thighs_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_knees_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_scarf_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_shirt_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_jacket_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_pants_megan Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_shoes_megan Green"

console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_thighs_megan_2 Green"
console.cmd cmd="Rogue.Item.SpawnInInventory brand_pb7_jacket_megan_2 Green"

console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_marksmanrifle_mosin_nagat_t2_v1 Orange"
console.cmd cmd="Rogue.Item.SpawnInInventory player_weapon_marksmanrifle_mosin_nagant_t2_v1 Orange"
console.cmd cmd="Rogue.Item.SpawnInInventory w_mod_rdsscope"

script.wait time="1"

console.cmd cmd="Rogue.EquipItem brand_pb7_face_megan"
console.cmd cmd="Rogue.EquipItem brand_pb7_chest_megan"
console.cmd cmd="Rogue.EquipItem brand_pb7_back_megan"
console.cmd cmd="Rogue.EquipItem brand_pb7_hands_megan"
console.cmd cmd="Rogue.EquipItem brand_pb7_thighs_megan"
console.cmd cmd="Rogue.EquipItem brand_pb7_knees_megan"
console.cmd cmd="Rogue.EquipItem brand_pb7_scarf_megan"
console.cmd cmd="Rogue.EquipItem brand_pb7_shirt_megan"
console.cmd cmd="Rogue.EquipItem brand_pb7_jacket_megan"
console.cmd cmd="Rogue.EquipItem brand_pb7_pants_megan"
console.cmd cmd="Rogue.EquipItem brand_pb7_shoes_megan"

script.end