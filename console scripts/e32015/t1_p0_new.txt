script.begin name="t1_p0_new.txt"
console.cmd cmd="Rogue.E32015.ConsoleScriptStart"
rogue.waitForInGame
console.cmd cmd="Rogue.DiscoverAllPSO"
console.cmd cmd="Rogue.Inventory.ClearStash"

console.cmd cmd="Rogue.SetLevel 12"
console.cmd cmd="Rogue.XP.Add 32730"

console.cmd cmd="Rogue.E32015.SetTeam 1"
console.cmd cmd="Rogue.E32015.SetMember 0"

console.cmd cmd="Rogue.CustomizeProfile isMale = true, values = { 3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.75, 0.5, 0.967213, 0, 0.5, 0.566926, 0, 0.5, 0.0867516, 0, 0, 0.525822, 0.530642, 0.885505, 0, 0, 0, 0, 0, }"
script.wait time="1"
console.cmd cmd="Rogue.CustomizeProfile isMale = true, values = { 3, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.75, 0.5, 0.967213, 0, 0.5, 0.566926, 0, 0.5, 0.0867516, 0, 0, 0.525822, 0.530642, 0.885505, 0, 0, 0, 0, 0, }"

console.cmd cmd="Rogue.DamageSelf -20000"

console.cmd cmd="Rogue.Item.SpawnInInventory access_resource_item_dark_zone_key"

console.cmd cmd="Rogue.Item.SpawnAndEquip brand_face_player_5 Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_chest_player_5 Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_back_player_5 Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_hands_player_5 Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_thighs_player_5 Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_knees_player_5 Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_scarf_player_5 Green"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_shirt_player_5 Green"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_pants_player_5 Green"
console.cmd cmd="Rogue.Item.SpawnAndEquip brand_shoes_player_5 Green"

console.cmd cmd="Rogue.Item.SpawnAndEquip Player_weapon_pistol_m9_t1_v1 Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip player_weapon_submachinegun_vector_t2_v1 Blue"
console.cmd cmd="Rogue.Item.SpawnAndEquip Player_weapon_assaultrifle_acr_t2_v1 Blue"

console.cmd cmd="Rogue.Item.SpawnInInventory w_mod_trijicon_rmr Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory w_mod_suppressor_9mm Blue"
console.cmd cmd="Rogue.Item.SpawnInInventory w_mod_holoscope Blue"

console.cmd cmd="Rogue.Inventory.SocketItem Player_weapon_assaultrifle_acr_t2_v1 w_mod_trijicon_rmr 1"
console.cmd cmd="Rogue.Inventory.SocketItem player_weapon_submachinegun_vector_t2_v1 w_mod_holoscope 1"
console.cmd cmd="Rogue.Inventory.SocketItem player_weapon_submachinegun_vector_t2_v1 w_mod_suppressor_9mm 2"

console.cmd cmd="Rogue.Talent.AssignSkillToLB player_pulse"
console.cmd cmd="Rogue.Talent.AssignSkillToRB player_first_aid"

console.cmd cmd="Rogue.E32015.ConsoleScriptEnd"
script.end