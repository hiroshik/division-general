////////////////////////////////////////////////////////////////////////////////////////
// Wipe inventory
////////////////////////////////////////////////////////////////////////////////////////
Rogue.Inventory.ClearAllButConsumables


////////////////////////////////////////////////////////////////////////////////////////
// Level 30 + All talents/skills/mods/...
////////////////////////////////////////////////////////////////////////////////////////
Rogue.Setlevel 30
Rogue.Featurerolloutui.Unlockall 1
Rogue.Baseofoperation.Buildallupgrades 1 


////////////////////////////////////////////////////////////////////////////////////////
// Assign skills and talents
////////////////////////////////////////////////////////////////////////////////////////

Rogue.Skill.AssignToLB player_turret
Rogue.Skill.AssignToRB player_seeker_mine
Rogue.SignatureSkill.SetOffensive
Rogue.Talent.FillSlotsWithRandom


////////////////////////////////////////////////////////////////////////////////////////
// Spawn 
////////////////////////////////////////////////////////////////////////////////////////

// Gears
Rogue.Item.SpawnInInventory player_back_offensive_generated_item Purple
Rogue.Item.SpawnInInventory player_chest_offensive_generated_item Purple
Rogue.Item.SpawnInInventory player_face_offensive_generated_item Purple
Rogue.Item.SpawnInInventory player_hands_offensive_generated_item Purple
Rogue.Item.SpawnInInventory player_knees_offensive_generated_item Purple
Rogue.Item.SpawnInInventory player_thighs_offensive_generated_item Purple

// Weapons
Rogue.Item.SpawnInInventory player_weapon_lightmachinegun_m60e4_t1_v1 Purple
Rogue.Item.SpawnInInventory player_weapon_marksmanrifle_m1a_t2_v1 Purple
Rogue.Item.SpawnInInventory player_weapon_submachinegun_mp5_t3_v1 Purple
Rogue.Item.SpawnInInventory player_weapon_assaultrifle_ACR_t1_v1 Purple
Rogue.Item.SpawnInInventory player_weapon_pistol_m9_t1_V1 Purple
Rogue.Item.SpawnInInventory player_weapon_shotgun_870_t1_V1 Purple

// Mods
Rogue.Item.SpawnInInventory w_mod_Suppressor_9mm Purple
Rogue.Item.SpawnInInventory w_mod_extendedmagazine1 Purple
Rogue.Item.SpawnInInventory w_mod_suppressor Purple
Rogue.Item.SpawnInInventory w_mod_tacticalgrip Purple
Rogue.Item.SpawnInInventory w_mod_reflexsight Purple
Rogue.Item.SpawnInInventory w_mod_heavybarrel Purple
Rogue.Item.SpawnInInventory w_mod_extendedmagazine1 Purple
Rogue.Item.SpawnInInventory w_mod_suppressor Purple
Rogue.Item.SpawnInInventory w_mod_ShortGrip_red Purple
Rogue.Item.SpawnInInventory w_mod_acogscope Purple
Rogue.Item.SpawnInInventory w_mod_heavybarrel Purple

Rogue.Item.SpawnInInventory player_gearmod_offensive_generated_item Purple
Rogue.Item.SpawnInInventory player_gearmod_offensive_generated_item Purple
Rogue.Item.SpawnInInventory player_gearmod_offensive_generated_item Purple
Rogue.Item.SpawnInInventory player_gearmod_offensive_generated_item Purple
Rogue.Item.SpawnInInventory player_gearmod_offensive_generated_item Purple
Rogue.Item.SpawnInInventory player_gearmod_offensive_generated_item Purple

Rogue.Item.SpawnInInventory player_gearmodskill_generated_item Purple
Rogue.Item.SpawnInInventory player_gearmodskill_generated_item Purple
Rogue.Item.SpawnInInventory player_gearmodskill_generated_item Purple

// Vanity
Rogue.Item.SpawnInInventory player_hat_a_1 Grey
Rogue.Item.SpawnInInventory player_jacket_a_1 Grey
Rogue.Item.SpawnInInventory player_pants_a_1 Grey
Rogue.Item.SpawnInInventory player_scarf_a_1 Grey
Rogue.Item.SpawnInInventory player_shirt_a_1 Grey
Rogue.Item.SpawnInInventory player_shoes_a_1 Grey


////////////////////////////////////////////////////////////////////////////////////////
// Socket 
////////////////////////////////////////////////////////////////////////////////////////

// Weapons
Rogue.Inventory.SocketItem player_weapon_pistol_m9_t1_V1 w_mod_Suppressor_9mm 1

Rogue.Inventory.SocketItem player_weapon_assaultrifle_ACR_t1_v1 w_mod_suppressor 1
Rogue.Inventory.SocketItem player_weapon_assaultrifle_ACR_t1_v1 w_mod_extendedmagazine1 1
Rogue.Inventory.SocketItem player_weapon_assaultrifle_ACR_t1_v1 w_mod_tacticalgrip 1
Rogue.Inventory.SocketItem player_weapon_assaultrifle_ACR_t1_v1 w_mod_reflexsight 1
Rogue.Inventory.SocketItem player_weapon_assaultrifle_ACR_t1_v1 w_mod_heavybarrel 1

Rogue.Inventory.SocketItem player_weapon_marksmanrifle_m1a_t2_v1 w_mod_suppressor 1
Rogue.Inventory.SocketItem player_weapon_marksmanrifle_m1a_t2_v1 w_mod_extendedmagazine1 1
Rogue.Inventory.SocketItem player_weapon_marksmanrifle_m1a_t2_v1 w_mod_ShortGrip_red 1
Rogue.Inventory.UnsocketItem player_weapon_marksmanrifle_m1a_t2_v1 w_mod_vx1scope
Rogue.Inventory.SocketItem player_weapon_marksmanrifle_m1a_t2_v1 w_mod_acogscope 1
Rogue.Inventory.SocketItem player_weapon_marksmanrifle_m1a_t2_v1 w_mod_heavybarrel 1


////////////////////////////////////////////////////////////////////////////////////////
// Equip 
////////////////////////////////////////////////////////////////////////////////////////

// Weapons
Rogue.SetWeaponInSlot player_weapon_assaultrifle_ACR_t1_v1 0
Rogue.SetWeaponInSlot player_weapon_marksmanrifle_m1a_t2_v1 1
Rogue.SetWeaponInSlot player_weapon_pistol_m9_t1_V1 2

Rogue.EquipItem player_weapon_pistol_m9_t1_V1
Rogue.EquipItem player_weapon_assaultrifle_ACR_t1_v1

// Gears
Rogue.EquipItem player_back_offensive_generated_item 
Rogue.EquipItem player_chest_offensive_generated_item 
Rogue.EquipItem player_face_offensive_generated_item 
Rogue.EquipItem player_hands_offensive_generated_item 
Rogue.EquipItem player_knees_offensive_generated_item 
Rogue.EquipItem player_thighs_offensive_generated_item

// Vanity
Rogue.EquipItem player_hat_a_1
Rogue.EquipItem player_jacket_a_1
Rogue.EquipItem player_pants_a_1
Rogue.EquipItem player_scarf_a_1
Rogue.EquipItem player_shirt_a_1
Rogue.EquipItem player_shoes_a_1