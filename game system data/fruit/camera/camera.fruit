include "modules/core/input/input.fruit"

// This enum should be kept in sync with the one in RClient_PlayerCameraData.h
enum PlayerCameraMode
{
	// Normal version of the cameras
	Default
	
	AimThrowable
	
	SprintCamera
	RunOOCCamera
	Roll
	LeapOverCover

	Downed
	Dead

	ArmadilloOutOfCover
	RiotShieldIdle
	RiotShieldWalk
	RiotShieldRun
	
	SkillLauncherDefaultIntro
	SkillLauncherRiotShieldDefaultIntro

	Ladder
	LadderSlide
	LadderManhole
	Vault
	VaultSprint
	Climb
    ClimbSprint
	Rope
	ZipLine
	Vendor
	Tent
	
	ShoulderAim
	CoverToCover

	LowCover
	LowCoverSuppressed
	LowCoverSuppressedBlindfire
	LowCoverAim
	LowCoverAimThrowable
	LowCoverAimCarWindow
	LowCornerCover
	LowCornerCoverSuppressed
	LowCornerCoverSuppressedBlindfire
	LowCornerCoverAim
	LowCornerCoverAimThrowable

	HighCover
	HighCoverSuppressed
	HighCoverAim
	HighCoverAimThrowable
	HighCornerCover
	HighCornerCoverSuppressed
	HighCornerCoverAim
	HighCornerCoverAimThrowable

	LowBackCover
	LowBackCoverSuppressed
	LowBackCoverSuppressedBlindfire
	LowBackCoverAim
	LowBackCoverAimThrowable
	LowBackCornerCover
	LowBackCornerCoverSuppressed
	LowBackCornerCoverSuppressedBlindfire
	LowBackCornerCoverAim
	LowBackCornerCoverAimThrowable

	HighBackCover
	HighBackCoverSuppressed
	HighBackCoverAim
	HighBackCoverAimThrowable
	HighBackCornerCover
	HighBackCornerCoverSuppressed
	HighBackCornerCoverAim
	HighBackCornerCoverAimThrowable

	Armadillo
	ArmadilloSuppressed
	ArmadilloSuppressedBlindfire
	ArmadilloAim
	ArmadilloAimThrowable
	ArmadilloBack
	ArmadilloBackSuppressed
	ArmadilloBackSuppressedBlindfire
	ArmadilloBackAim
	ArmadilloBackAimThrowable
	
	RiotShieldAiming
	
	SkillLauncherLowCoverIntro
	SkillLauncherLowCoverCornerIntro
	SkillLauncherHighCoverIntro
	SkillLauncherHighCoverCornerIntro

    Scope
    ScopeLowCover
    ScopeLowCornerCover
    ScopeLowCoverBack
	ScopeLowCoverCarWindow
    ScopeHighCornerCover
    ScopeHighCover

	ShoulderAimMirror
	CoverToCoverMirror

	LowCoverMirror
	LowCoverSuppressedMirror
	LowCoverSuppressedBlindfireMirror
	LowCoverAimMirror
	LowCoverAimThrowableMirror
	LowCoverAimCarWindowMirror
	LowCornerCoverMirror
	LowCornerCoverSuppressedMirror
	LowCornerCoverSuppressedBlindfireMirror
	LowCornerCoverAimMirror
	LowCornerCoverAimThrowableMirror

	HighCoverMirror
	HighCoverSuppressedMirror
	HighCoverAimMirror
	HighCoverAimThrowableMirror
	HighCornerCoverMirror
	HighCornerCoverSuppressedMirror
	HighCornerCoverAimMirror
	HighCornerCoverAimThrowableMirror

	LowBackCoverMirror
	LowBackCoverSuppressedMirror
	LowBackCoverSuppressedBlindfireMirror
	LowBackCoverAimMirror
	LowBackCoverAimThrowableMirror
	LowBackCornerCoverMirror
	LowBackCornerCoverSuppressedMirror
	LowBackCornerCoverSuppressedBlindfireMirror
	LowBackCornerCoverAimMirror
	LowBackCornerCoverAimThrowableMirror

	HighBackCoverMirror
	HighBackCoverSuppressedMirror
	HighBackCoverAimMirror
	HighBackCoverAimThrowableMirror
	HighBackCornerCoverMirror
	HighBackCornerCoverSuppressedMirror
	HighBackCornerCoverAimMirror
	HighBackCornerCoverAimThrowableMirror

	ArmadilloMirror
	ArmadilloSuppressedMirror
	ArmadilloSuppressedBlindfireMirror
	ArmadilloAimMirror
	ArmadilloAimThrowableMirror
	ArmadilloBackMirror
	ArmadilloBackSuppressedMirror
	ArmadilloBackSuppressedBlindfireMirror
	ArmadilloBackAimMirror
	ArmadilloBackAimThrowableMirror
	
	RiotShieldAimingMirror
    
	SkillLauncherLowCoverIntroMirror	
	SkillLauncherLowCoverCornerIntroMirror
	SkillLauncherHighCoverIntroMirror	
	SkillLauncherHighCoverCornerIntroMirror

    ScopeMirror
    ScopeLowCoverMirror
    ScopeLowCornerCoverMirror
    ScopeLowCoverBackMirror
	ScopeLowCoverCarWindowMirror
    ScopeHighCornerCoverMirror
    ScopeHighCoverMirror
	
	DefaultIndoor
	
	AimThrowableIndoor
	
	SprintCameraIndoor
	RunOOCCameraIndoor
	RollIndoor
	LeapOverCoverIndoor
	
	DownedIndoor
	DeadIndoor

	ArmadilloOutOfCoverIndoor
	RiotShieldIdleIndoor
	RiotShieldWalkIndoor
	RiotShieldRunIndoor
	
	SkillLauncherDefaultIntroIndoor
	SkillLauncherRiotShieldDefaultIntroIndoor

 	LadderIndoor
	LadderSlideIndoor
	LadderManholeIndoor
	VaultIndoor
	VaultSprintIndoor
	ClimbIndoor
    ClimbSprintIndoor
	RopeIndoor
	ZipLineIndoor
	VendorIndoor
	TentIndoor

	ShoulderAimIndoor
	CoverToCoverIndoor

	LowCoverIndoor
	LowCoverSuppressedIndoor
	LowCoverSuppressedBlindfireIndoor
	LowCoverAimIndoor
	LowCoverAimThrowableIndoor
	LowCoverAimCarWindowIndoor
	LowCornerCoverIndoor
	LowCornerCoverSuppressedIndoor
	LowCornerCoverSuppressedBlindfireIndoor
	LowCornerCoverAimIndoor
	LowCornerCoverAimThrowableIndoor

	HighCoverIndoor
	HighCoverSuppressedIndoor
	HighCoverAimIndoor
	HighCoverAimThrowableIndoor
	HighCornerCoverIndoor
	HighCornerCoverSuppressedIndoor
	HighCornerCoverAimIndoor
	HighCornerCoverAimThrowableIndoor

	LowBackCoverIndoor
	LowBackCoverSuppressedIndoor
	LowBackCoverSuppressedBlindfireIndoor
	LowBackCoverAimIndoor
	LowBackCoverAimThrowableIndoor
	LowBackCornerCoverIndoor
	LowBackCornerCoverSuppressedIndoor
	LowBackCornerCoverSuppressedBlindfireIndoor
	LowBackCornerCoverAimIndoor
	LowBackCornerCoverAimThrowableIndoor

	HighBackCoverIndoor
	HighBackCoverSuppressedIndoor
	HighBackCoverAimIndoor
	HighBackCoverAimThrowableIndoor
	HighBackCornerCoverIndoor
	HighBackCornerCoverSuppressedIndoor
	HighBackCornerCoverAimIndoor
	HighBackCornerCoverAimThrowableIndoor

	ArmadilloIndoor
	ArmadilloSuppressedIndoor
	ArmadilloSuppressedBlindfireIndoor
	ArmadilloAimIndoor
	ArmadilloAimThrowableIndoor
	ArmadilloBackIndoor
	ArmadilloBackSuppressedIndoor
	ArmadilloBackSuppressedBlindfireIndoor
	ArmadilloBackAimIndoor
	ArmadilloBackAimThrowableIndoor
	
	RiotShieldAimingIndoor
	
	SkillLauncherLowCoverIntroIndoor	
	SkillLauncherLowCoverCornerIntroIndoor
	SkillLauncherHighCoverIntroIndoor	
	SkillLauncherHighCoverCornerIntroIndoor

    ScopeIndoor
    ScopeLowCoverIndoor
    ScopeLowCornerCoverIndoor
    ScopeLowCoverBackIndoor
	ScopeLowCoverCarWindowIndoor
    ScopeHighCornerCoverIndoor
    ScopeHighCoverIndoor

	ShoulderAimIndoorMirror
	CoverToCoverIndoorMirror

	LowCoverIndoorMirror
	LowCoverSuppressedIndoorMirror
	LowCoverSuppressedBlindfireIndoorMirror
	LowCoverAimIndoorMirror
	LowCoverAimThrowableIndoorMirror
	LowCoverAimCarWindowIndoorMirror
	LowCornerCoverIndoorMirror
	LowCornerCoverSuppressedIndoorMirror
	LowCornerCoverSuppressedBlindfireIndoorMirror
	LowCornerCoverAimIndoorMirror
	LowCornerCoverAimThrowableIndoorMirror

	HighCoverIndoorMirror
	HighCoverSuppressedIndoorMirror
	HighCoverAimIndoorMirror
	HighCoverAimThrowableIndoorMirror
	HighCornerCoverIndoorMirror
	HighCornerCoverSuppressedIndoorMirror
	HighCornerCoverAimIndoorMirror
	HighCornerCoverAimThrowableIndoorMirror

	LowBackCoverIndoorMirror
	LowBackCoverSuppressedIndoorMirror
	LowBackCoverSuppressedBlindfireIndoorMirror
	LowBackCoverAimIndoorMirror
	LowBackCoverAimThrowableIndoorMirror
	LowBackCornerCoverIndoorMirror
	LowBackCornerCoverSuppressedIndoorMirror
	LowBackCornerCoverSuppressedBlindfireIndoorMirror
	LowBackCornerCoverAimIndoorMirror
	LowBackCornerCoverAimThrowableIndoorMirror

	HighBackCoverIndoorMirror
	HighBackCoverSuppressedIndoorMirror
	HighBackCoverAimIndoorMirror
	HighBackCoverAimThrowableIndoorMirror
	HighBackCornerCoverIndoorMirror
	HighBackCornerCoverSuppressedIndoorMirror
	HighBackCornerCoverAimIndoorMirror
	HighBackCornerCoverAimThrowableIndoorMirror

	ArmadilloIndoorMirror
	ArmadilloSuppressedIndoorMirror
	ArmadilloSuppressedBlindfireIndoorMirror
	ArmadilloAimIndoorMirror
	ArmadilloAimThrowableIndoorMirror
	ArmadilloBackIndoorMirror
	ArmadilloBackSuppressedIndoorMirror
	ArmadilloBackSuppressedBlindfireIndoorMirror
	ArmadilloBackAimIndoorMirror
	ArmadilloBackAimThrowableIndoorMirror
	
	RiotShieldAimingIndoorMirror
	
	SkillLauncherLowCoverIntroIndoorMirror
	SkillLauncherLowCoverCornerIntroIndoorMirror
	SkillLauncherHighCoverIntroIndoorMirror	
	SkillLauncherHighCoverCornerIntroIndoorMirror

    ScopeIndoorMirror
    ScopeLowCoverIndoorMirror
    ScopeLowCornerCoverIndoorMirror
    ScopeLowCoverBackIndoorMirror
	ScopeLowCoverCarWindowIndoorMirror
    ScopeHighCornerCoverIndoorMirror
    ScopeHighCoverIndoorMirror
}

class CameraPreset < uid=auto >
{
	PlayerCameraMode myMode

	float myFoV	90.0
	float myDistance 3.0
	
	float myMinPitch -90.0
	float myMaxPitch  90.0
	
	float myDefaultPitch 0.0
	
	float myMaxYawAcceleration 0.07
	float myMaxYawVelocity 0.1
	
	float myMaxPitchAcceleration 0.03
	float myMaxPitchVelocity 0.05
	
	float myDecelerationTime 0.25
	string myDecelerationCurve "{ defaultLeftSlope = 0, defaultRightSlope = 0, defaultValue = 0, interpolator = 0, isRefinedSampling = true, keys2 = { inTangents = { -0 -0, -0.270235 -0.002532, }, outTangents = { 0.420993 0.814721, 0 0, }, properties = { 275, 1299, }, values = { 0 0, 1 1, }, }, numKeys = 2, }" < bhv=inputgraph >
		
	vec3f myPivot 1.0;1.75;0.0
	bool myBlendPivotWithMirror TRUE < tooltip="Set this to false to prevent this camera mode from blending its pivot with its mirror mode's pivot." >
	bool myCanRaiseWhenCloseToTarget TRUE < tooltip="Set this to false to prevent the camera from raising up when the camera is close to the target." >
	float myPivotMirrorSpringStrength 100.0 < tooltip="How smoothly the pivot moves when interpolating. High value means less smooth." >
	
	InputAxisSensitivity mySensitivityX
	InputAxisSensitivity mySensitivityY
	
	bool myUseSynchedInputAxes FALSE
	string myAxisSynchedInputScaleCurve "{ numKeys = 2, interpolator = 0, defaultValue = 0, defaultLeftSlope = 0, keys = { properties = { -749, -749, }, values = { 0 0, 1 1, }, inTangents = { -0 -0, -0.279596 -0.8, }, outTangents = { 0.7 0.038516, 0 0, }, }, defaultRightSlope = 0, }" < bhv=inputgraph >
	string myAxisSynchedInputRemapCurve "{ defaultLeftSlope = 0, defaultRightSlope = 0, defaultValue = 0, interpolator = 0, isRefinedSampling = true, keys2 = { inTangents = { 0 0, -30 -30, -5 0, }, outTangents = { 15 0, 30 30, 0 -0, }, properties = { 275, 2323, 1299, }, values = { 0 0, 45 45, 90 90, }, }, numKeys = 3, }" < bhv=inputgraph >
	string myAxisSynchedAccelerationCurve "{ numKeys = 2, interpolator = 0, defaultValue = 0, defaultLeftSlope = 0, keys = { properties = { -749, -749, }, values = { 0 0, 1 1, }, inTangents = { -0 -0, -0.279596 -0.1, }, outTangents = { 0.5 0.0, 0 0, }, }, defaultRightSlope = 0, }" < bhv=inputgraph >
	float myAxisSynchedAccelerationTime 1.0
	float myAxisSynchedMaxSpeed 0.0

    float myMouseSensitivityMultiplier 1.0

	bool  myUseEyeTracking     FALSE
	float myEyeScreenThreshold 0.45
	float myEyeSensitivityX    0.6
	float myEyeSensitivityY    0.3

    float myTurnSpeedCapX 0.0
    float myTurnSpeedCapY 0.0
	
	float myOverrideCollisionAnchorY -1.0 < tooltip="Normally the collision anchor is at the pivot height, This overrides it. -1 = no override" >
}

list CameraPresetList
{
	CameraPreset
}

list PlayerCameraModeList
{
	PlayerCameraMode
}

class CameraTransition
{
	PlayerCameraModeList myFrom
	PlayerCameraModeList myTo
	float myDuration
	string myCurve "{ defaultLeftSlope = 0, defaultRightSlope = 0, defaultValue = 0, interpolator = 0, keys = { inTangents = { -0 -0, -0.602639 -0.038683, }, outTangents = { 0.307335 0.942354, 0 0, }, properties = { -749, -749, }, values = { 0 0, 1 1, }, }, numKeys = 2, }" < bhv=inputgraph >
	int myMaxActiveBlends -1
	bool myAcceptMirrorTransition FALSE < tooltip="This needs to be set to TRUE if you want a transition between a non-mirrored and a mirrored state." >
}

list CameraTransitionOverrides
{
	CameraTransition
}

list CameraTransitionSpecials
{
	CameraTransition
}

class CameraShakeBlock
{
	bool myIsCyclic FALSE
	float myBlendInTime 0.0
	float myBlendOutTime 0.0
	float myDuration 0.0
	float myStartOffset 0.0

	string myPitchMoveCurve "" < bhv=inputgraph >
	string myPitchRotateCurve "" < bhv=inputgraph >
	string myYawMoveCurve "" < bhv=inputgraph >
	string myYawRotateCurve "" < bhv=inputgraph >
	string myDistanceCurve "" < bhv=inputgraph >
	string myRollCurve "" < bhv=inputgraph >
	string myFovCurve "" < bhv=inputgraph >

	float myPitchMoveAmplitude 0.0
	float myPitchRotateAmplitude 0.0
	float myYawMoveAmplitude 0.0
	float myYawRotateAmplitude 0.0
	float myDistanceAmplitude 0.0
	float myRollAmplitude 0.0	
	float myFovAmplitude 0.0 < tooltip="Angle added to the camera's field of vision in degrees." >
}

list CameraShakeBlockList
{	
	CameraShakeBlock
}

class CameraShake
{
	string myName ""
	CameraShakeBlockList myBlocks
}

list CameraShakeList
{
	CameraShake
}

class CameraShakeDirectional
{
	float mySwingArmAngle < bhv=angle tooltip="Max angle for the displacement curve when a swing arm shake is active." >
	float mySwingArmLength < tooltip="The distance between the camera and its pivot point." >
	float myPitchAngle < bhv=angle tooltip="Max angle for the displacement curve when a pitch shake is active." >
	float myDuration < tooltip="The total duration (in seconds) of a shake." >
	float myCooldown < tooltip="The cooldown (in seconds) until another shake can take effect." >
	string myDisplacement 0.0 < bhv=inputgraph tooltip="Pitch and swing arm angle scaling over time. X = scalar. Y = scalar" >
	string myVariantRatio 0.0 < bhv=inputgraph tooltip="The ratio of swing arm shake versus pitch shake. X = degrees between camera and source (0-90). Y = scalar." >
	string myFovScale 0.0 < bhv=inputgraph tooltip="How much shake that should be applied at different FoV values. X = degrees. Y = scalar." >
}

class CameraShakeConfig
{
	CameraShakeDirectional myDirectionalShake
	CameraShakeList myPresetsList
}

class SocialModeTargetSnapData
{
	float myMaxAimDistance 0.0
	float myMaxTargetRayDistToTestCamDist 0.0
	float myTrackingSpringStrength 0.0
	float myAimingSpringStrength 0.0
	float myAimingBaseSpeed 0.0
	
	float mySnapBoxSizeX 0.0
	float mySnapBoxSizeY 0.0
	
	float myLockTargetCenterOffsetX 0.0
	float myLockTargetCenterOffsetY 0.0
	float myLockTargetCenterOffsetZ 0.0
}

class Camera
{
	float myMouseSensitivityX
	float myMouseSensitivityY				
	
	float myPitchRecoveryStrength 10.0 < min=1 max=100 >
	float myPitchRecoveryDelay 2.5

	float myPivotMirrorStartAngle 0.3 < tooltip="At this angle we start interpolating towards the mirror mode pivot. You can disable this feature by setting this value to the same as myPivotMirrorStopAngle." >
	float myPivotMirrorStopAngle 1.0 < tooltip="At this angle we will reach the interpolation target." >
	float myPivotMirrorTargetRatio 0.9 < tooltip="How far towards the mirror pivot position we will interpolate. So if this is 0.9 we will have moved 90% towards the mirror pivot when we reach the value for myPivotMirrorStopAngle and then it will stop interpolating." >
	string myPivotMovementRemapCurve "{ defaultLeftSlope = 0, defaultRightSlope = 0, defaultValue = 0, interpolator = 0, isRefinedSampling = true, keys2 = { inTangents = { 0 0, -0.3 -0.3, -0.2 0, }, outTangents = { 0.2 0, 0.3 0.3, 0 -0, }, properties = { 275, 2323, 1299, }, values = { 0 0, 0.5 0.5, 1 1, }, }, numKeys = 3, }" < bhv=inputgraph >
	float myPivotCornerExtentionStartAngle 0.1 < tooltip="At this angle we start interpolating towards the corner extended pivot. You can disable this feature by setting this value to the same as myPivotCornerExtentionStopAngle." >
	float myPivotCornerExtentionStopAngle 1.57 < tooltip="At this angle we will reach the interpolation target." >
	float myPivotCornerExtentionDistance  0.75 < tooltip="How far beyond normal should we push the camera out in corners." >

	string myImmersiveSkyFovCurve "{ defaultLeftSlope = 0, defaultRightSlope = 0, defaultValue = 0, interpolator = 0, isRefinedSampling = true, keys2 = { inTangents = { -0 0, -0.2 0, }, outTangents = { 0.641985 -0, 0 0, }, properties = { 275, 1299, }, values = { 0 0, 1 1, }, }, numKeys = 2, }" < bhv=inputgraph >
	float myImmersiveSkyStartAngle 45.0 < tooltip="Minimum angle zenith for performing an immersive sky FoV change in degrees." >
	float myImmersiveSkyStopAngle 60.0 < tooltip="Zenith angle where the immersive sky FoV change stops in degrees." >
	float myImmersiveSkyMaxFov 5.0 < tooltip="Maximum amount of degrees added to the current FoV when looking up at the sky." >
	
	string myPitchCollisionAvoidanceCurve "{ defaultLeftSlope = 0, defaultRightSlope = 0, defaultValue = 0, interpolator = 0, isRefinedSampling = true, keys2 = { inTangents = { -0 0, -0.2 0, }, outTangents = { 0.641985 -0, 0 0, }, properties = { 275, 1299, }, values = { 0 0, 1 1, }, }, numKeys = 2, }" < bhv=inputgraph >
	float myPitchCollisionAvoidanceStartAngle 5.15 < tooltip="Minimum angle zenith for the camera to change rotation pivot when looking up to avoid the camera colliding with the ground." >
	float myPitchCollisionAvoidancePivotZ 0.15 < tooltip="How far to move the camera pivot in Z, added to the camera preset pivot when looking upwards to avoid the camera colliding with the ground." >
	float myPitchCollisionAvoidanceDistance 2.35 < tooltip="The max distance to the camera pivot allowed when looking upwards to avoid the camera colliding with the ground." >

	float myAimPointRetentionMaxRotation 10.0f < tooltip="Maximum rotation change when shoulder switching" >
	float myAimPointRetentionFadeOutDistanceOOC 7.0 < tooltip="Distance below which aim point retention will diminish when shoulder switching out of cover" >
	float myAimPointRetentionFadeOutDistanceCover 4.0 < tooltip="Distance below which aim point retention will diminish when shoulder switching in cover" >
	float myAimPointRetentionFadeOutDistanceTarget 0.5 < tooltip="Distance below which aim point retention will diminish when shoulder switching and aiming at a target" >
	string myAimPointRetentionFadeCurve "{ defaultLeftSlope = 0, defaultRightSlope = 0, defaultValue = 0, interpolator = 0, isRefinedSampling = true, keys2 = { inTangents = { -0 0, -0.2 0, }, outTangents = { 0.641985 -0, 0 0, }, properties = { 275, 1299, }, values = { 0 0, 1 1, }, }, numKeys = 2, }" < bhv=inputgraph >
	
	float myRaiseTriggerDistance 2.0 < tooltip="If the distance between the target and camera is below this value the camera will starts to raise." >
	float myRaiseOffset 0.1 < tooltip="How much the camera will raise when at a distance of 0 to the target point." >
	string myRaiseCurve "{ defaultLeftSlope = 0, defaultRightSlope = 0, defaultValue = 0, interpolator = 0, isRefinedSampling = true, keys2 = { inTangents = { -0 0, -0.2 0, }, outTangents = { 0.2 0, 0 0, }, properties = { 275, 1299, }, values = { 0 0, 1 1, }, }, numKeys = 2, }" < bhv=inputgraph >
	float myRaiseCoverAnchorOffset -0.7 < tooltip="When in cover the camera raise anchor will be offset towards the cover by this value. Should be negative or the anchor will be offset away from the cover." >
	float myCollisionHistoryTime 0.2 < tooltip="How long should we use the collision distance before trying to blend out." >
	float myCollisionOutSpringStrength 15.0 < tooltip="How smoothly the camera returns to its no collision position. High value means less smooth." >
	float myCollisionOutSpringStrengthAim 70.0 < tooltip="How smoothly the camera returns to its no collision position. High value means less smooth." >
	float myCollisionFanMinScale 0.3 < tooltip="When scaling the collision fan based on the speed what is the minimum scale." >
	float myCollisionFanMaxScale 1.0 < tooltip="When scaling the collision fan based on the speed what is the maximum scale." >
	float myCollisionFanMaxRotationSpeed 2.0 < tooltip="When scaling the collision fan based on the rotation what is the rotation speed when the scale becomes myCollisionFanMinScale" >

	float myCharacterInvisibilityLimit 0.2f < tooltip="The distance at which the player character becomes invisible to avoid clipping and obstruction out ot aim" >
	float myCharacterInvisibilityLimitAim 0.45f < tooltip="The distance at which the player character becomes invisible to avoid clipping and obstruction when in aim" >
	float myEnterFirstPersonModeXLimit 0.2 < tooltip="How close to the head in local space X can we come before entering first person mode" >
	float myExitFirstPersonModeXLimit 0.3 < tooltip="How far from the head in local space X can we come before exiting first person mode" >
	float myFirstPersonDistance 0.2 < tooltip="Distance to head in first person mode" >
	float myEnterFirstPersonSpringConstant 200 < tooltip="The spring constant when entering forst person mode" >
	float myShoulderSwitchLimitRatio 1.3 < tooltip="How much better must the opposite shoulder be for us to make the switch?" >
	float myShoulderSwitchCutoffXDistance 0.25 < tooltip="The distance along the local X axis beyond which we will not switch shoulders out of cover" >
	float myShoulderSwitchCutoffXDistanceCover 0.35 < tooltip="The distance along the local X axis beyond which we will not switch shoulders in cover" >
	
	float myNearPlane 0.01
	float myFarPlane 2500
	
	float myLookAtMaxDistWS 70.0 < tooltip="Aim point will not be perfectly retained beyond this distance." >

	float myInvisibleWallDistance 0.5 < tooltip="Minimum camera distance to the player when affected by a low cover invisible wall." >
	float myInvisibleWallSpringStrength 700.0 < tooltip="Strength of the spring for entering/exiting the distance limit for the invisible wall in low covers." >
	
	bool myHideEnemiesWhenClose false
	bool myHideNPCsWhenClose false
	float myHideAgentsRadius 0.4

	SocialModeTargetSnapData mySocialModeTargetSnapData 
	
	CameraShake mySprintShake
	CameraShake myRollShake
	CameraShake myOnDamagedShake
	CameraShake myGoIntoCoverShake
	CameraShake myLeaveCoverShake
	CameraShake myLeapOverShake
	CameraShake mySprintBrakeShake
	CameraShake myOnNearMissShake
	CameraShake mySuppressionShake
	CameraShake myRegularStaggerShake
	CameraShake myLargeStaggerShake
	CameraShake myMeleePushHitShake

	string mySuppressionShakeCurve 0.0 < bhv=inputgraph >
	
	CameraTransition myIndoorOutdoorTransition
	CameraTransitionOverrides myTransitionOverrides
	CameraTransitionSpecials mySpecialTransitions

	string myYAxisSensitivityMultiplierCurve "{ defaultLeftSlope = 0, defaultRightSlope = 0, defaultValue = 0, interpolator = 0, isRefinedSampling = true, keys2 = { inTangents = { -0 -0, -0.2 0, }, outTangents = { 0.00155663 0.285549, 0 0, }, properties = { 275, 1299, }, values = { 0 0.00321889, 1 1, }, }, numKeys = 2, }" < bhv=inputgraph >
	string myGroupSpectatorTransitionCurve  "{ defaultLeftSlope = 0, defaultRightSlope = 0, defaultValue = 0, interpolator = 0, keys = { inTangents = { -0 -0, -0.602639 -0.038683, }, outTangents = { 0.307335 0.942354, 0 0, }, properties = { -749, -749, }, values = { 0 0, 1 1, }, }, numKeys = 2, }" < bhv=inputgraph >
}
