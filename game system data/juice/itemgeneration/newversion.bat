set /p oldCodeVersion="old code version: "
set /p oldDataVersion="old data version: "

set /p newCodeVersion="new code version: "
set /p newDataVersion="new data version: "

set pattern=code%oldCodeVersion%_data%oldDataVersion%_
set replace=code%newCodeVersion%_data%newDataVersion%_

set folders=exclusions filenames itemnames itemweights powerlevels qualitybonuses subtypes improving

setlocal enabledelayedexpansion

for %%a in (%folders%) do (
	copy %%a\code%oldCodeVersion%_data%oldDataVersion%_*  code%oldCodeVersion%_data%oldDataVersion%_*
	for %%# in ("code%oldCodeVersion%_data%oldDataVersion%_*") do (
		set "File=%%~nx#"
		ren "%%#" "!File:%pattern%=%replace%!"
	)
	move code%newCodeVersion%_data%newDataVersion%_* %%a  
)

pause&exit

