include "rogue/game system data/juice/input/inputactions.juice"
include "rogue/game system data/fruit/input/gameinput.fruit"

TriggerGameAction "[Cover] Exit Cover"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "A Cross Press And Release Before 200ms"
				{
					myInputActionType PressAndReleaseBeforeTime
					myTimer 200
					myInputAction = A_CROSS
				}
				ConditionalInputAction "KB Input"
				{
					myInputActionType PressAndReleaseBeforeTime
					myTimer 200
					myInputAction = KB_ACTION_1
				}
			}
		}
		ConditionalGameActionState "In Cover"
		{
			myGameActionState InCover
		}
		ConditionalGameActionState "Not In Slide"
		{
			myNegate TRUE
			myGameActionState Sliding
		}
		ConditionalGameActionState "Can Exit Cover"
		{
			myGameActionState CanExitCover
		}
	}
	myGameAction ExitCover
}

TriggerGameAction "[Cover] Highlight Cover To Cover Mode"
{
	myConditionalInputActions
	{
		ConditionalGameActionState "In Cover"
		{
			myNegate FALSE
			myGameActionState InCover
		}
		ConditionalGameActionState "Not Aiming"
		{
			myNegate TRUE
		}
		ConditionalGameActionState CoverToCoverAllowed
		{
			myGameActionState CoverToCoverIsAllowed
		}
		ConditionalGameActionState "Not Running Prevented"
		{
			myNegate TRUE
			myGameActionState IsRunningPrevented
		}
		ConditionalGameActionState "Not In Slide"
		{
			myNegate TRUE
			myGameActionState Sliding
		}
	}
	myGameAction HighlightCoverToCover
}

TriggerGameAction "[Cover] Begin Cover To Cover"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "A Cross Pressed After 200ms"
				{
					myInputActionType PressedAfterTime
					myTimer 200
					myInputAction = A_CROSS
				}
				ConditionalInputAction "KB Input"
				{
					myInputActionType PressedAfterTime
					myTimer 200
					myInputAction = KB_ACTION_1
				}
			}
		}
		ConditionalGameAction "Highlight Cover To Cover"
		{
			myRequireNewInput TRUE
			myGameAction HighlightCoverToCover
		}
		ConditionalGameActionState "Cover To Cover Is Available"
		{
			myGameActionState CoverToCoverIsAvailable
		}
		ConditionalGameActionState "Not Running Prevented"
		{
			myNegate TRUE
			myGameActionState IsRunningPrevented
		}
		ConditionalGameActionState "Not In Slide"
		{
			myNegate TRUE
			myGameActionState Sliding
		}
		ConditionalGameActionState "Not In Cover To Cover"
		{
			myNegate TRUE
			myGameActionState InCoverToCover
		}
	}
	myGameAction BeginCoverToCover
}

TriggerGameAction "[Cover] End Cover To Cover"
{
	myConditionalInputActions
	{
		ConditionalInputAction "A Cross Not Hold Down"
		{
			myInputActionType HoldDown
			myNegate TRUE
			myInputAction = A_CROSS
		}
		ConditionalInputAction "KB Input"
		{
			myInputActionType HoldDown
			myNegate TRUE
			myInputAction = KB_ACTION_1
		}
		ConditionalGameActionState "In Cover To Cover"
		{
			myGameActionState InCoverToCover
		}
	}
	myGameAction EndCoverToCover
}

TriggerGameAction "[Cover] End Cover To Cover (Because Running Is Prevented)"
{
	myConditionalInputActions
	{
		ConditionalGameActionState "Running Prevented"
		{
			myGameActionState IsRunningPrevented
		}
		ConditionalGameActionState "In Cover To Cover"
		{
			myGameActionState InCoverToCover
		}
	}
	myGameAction EndCoverToCover
}

TriggerGameAction "[Cover] Enter Cover"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "A/Cross Hold"
				{
					myInputActionType HoldDown
					myInputAction = A_CROSS
				}
				ConditionalInputAction "KB Input"
				{
					myInputActionType HoldDown
					myInputAction = KB_ACTION_1
				}
			}
		}
		ConditionalGameActionState "Cover Is Available"
		{
			myGameActionState CoverIsAvailable
		}
		ConditionalGameActionState "Not In Cover"
		{
			myNegate TRUE
			myGameActionState InCover
		}
		ConditionalGameActionState "Not In Slide"
		{
			myNegate TRUE
			myGameActionState Sliding
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
		ConditionalGameActionState "Not Bleeding Out"
		{
			myNegate TRUE
			myGameActionState IsBleedingOut
		}
		ConditionalGameActionState "Not In Cover To Cover"
		{
			myNegate TRUE
			myRequireNewInput TRUE
			myGameActionState InCoverToCover
		}
		ConditionalGameActionState "Not in Safe Zone"
		{
			myNegate TRUE
			myGameActionState IsInSafeZone
		}
	}
	myGameAction EnterCover
}

TriggerGameAction "[Cover] Highlight Right Cover Corner"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalRotatedInputAction "Rotated Left Stick Right Hold Down"
				{
					myInputActionType HoldDown
					myForwardInputAction = LEFT_STICK_UP
					myBackInputAction = LEFT_STICK_DOWN
					myLeftInputAction = LEFT_STICK_LEFT
					myRightInputAction = LEFT_STICK_RIGHT
					myExpectedInputAction = LEFT_STICK_RIGHT
				}
				ConditionalRotatedInputAction "KB Right Hold Down"
				{
					myInputActionType HoldDown
					myForwardInputAction = KB_MOVE_UP
					myBackInputAction = KB_MOVE_DOWN
					myLeftInputAction = KB_MOVE_LEFT
					myRightInputAction = KB_MOVE_RIGHT
					myExpectedInputAction = KB_MOVE_RIGHT
				}
			}
		}
		ConditionalGameActionState "In Cover"
		{
			myGameActionState InCover
		}
		ConditionalGameActionState "Is In Corner Cover"
		{
			myGameActionState IsCornerCoverAvailable
		}
		ConditionalGameActionState "Not Aiming"
		{
			myNegate TRUE
		}
	}
	myGameAction IsCoverAroundCornerRightAvailable
}

TriggerGameAction "[Cover] Highlight Horizontal Swap Right"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalRotatedInputAction "Rotated Left Stick Right Hold Down"
				{
					myInputActionType HoldDown
					myForwardInputAction = LEFT_STICK_UP
					myBackInputAction = LEFT_STICK_DOWN
					myLeftInputAction = LEFT_STICK_LEFT
					myRightInputAction = LEFT_STICK_RIGHT
					myExpectedInputAction = LEFT_STICK_RIGHT
				}
				ConditionalRotatedInputAction "KB Right Hold Down"
				{
					myInputActionType HoldDown
					myForwardInputAction = KB_MOVE_UP
					myBackInputAction = KB_MOVE_DOWN
					myLeftInputAction = KB_MOVE_LEFT
					myRightInputAction = KB_MOVE_RIGHT
					myExpectedInputAction = KB_MOVE_RIGHT
				}
			}
		}
		ConditionalGameActionState "In Cover"
		{
			myGameActionState InCover
		}
		ConditionalGameActionState "Is In Corner Cover"
		{
			myGameActionState IsCornerCoverAvailable
		}
		ConditionalGameActionState "Not Aiming"
		{
			myNegate TRUE
		}
	}
	myGameAction IsHorizontalSwapCoverRightAvailable
}

TriggerGameAction "[Cover] Take Cover Around Right Corner"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Option Based Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalActionGroup Default
				{
					myOperator And
					myConditionalInputActions
					{
						ConditionalActionGroup "Input Actions"
						{
							myOperator Or
							myConditionalInputActions
							{
								ConditionalRotatedInputAction "Rotated Left Stick Right Hold Down"
								{
									myInputActionType HoldDownAfterTime
									myTimer 400
									myForwardInputAction = LEFT_STICK_UP
									myBackInputAction = LEFT_STICK_DOWN
									myLeftInputAction = LEFT_STICK_LEFT
									myRightInputAction = LEFT_STICK_RIGHT
									myExpectedInputAction = LEFT_STICK_RIGHT
								}
								ConditionalRotatedInputAction "KB Right Hold Down"
								{
									myInputActionType HoldDownAfterTime
									myTimer 400
									myForwardInputAction = KB_MOVE_UP
									myBackInputAction = KB_MOVE_DOWN
									myLeftInputAction = KB_MOVE_LEFT
									myRightInputAction = KB_MOVE_RIGHT
									myExpectedInputAction = KB_MOVE_RIGHT
								}
							}
						}
						ConditionalGameActionState "In Cover"
						{
							myGameActionState InCover
						}
						ConditionalGameActionState "Is In Corner Cover"
						{
							myGameActionState IsCornerCoverAvailable
						}
						ConditionalGameActionState "Corner Timer Expired"
						{
							myGameActionState CornerTimerHasExpired
						}
						ConditionalGameActionState "Button Option Not Set"
						{
							myNegate TRUE
							myGameActionState MoveAroundCornerUsingButtonPress
						}
					}
				}
				ConditionalActionGroup ButtonPress
				{
					myConditionalInputActions
					{
						ConditionalGameActionState "In Cover"
						{
							myGameActionState InCover
						}
						ConditionalGameActionState "Is In Corner Cover"
						{
							myGameActionState IsCornerCoverAvailable
						}
						ConditionalActionGroup "Input Actions"
						{
							myOperator Or
							myConditionalInputActions
							{
								ConditionalActionGroup Gamepad
								{
									myConditionalInputActions
									{
										ConditionalRotatedInputAction "Rotated Left Stick Right Hold Down"
										{
											myInputActionType HoldDown
											myForwardInputAction = LEFT_STICK_UP
											myBackInputAction = LEFT_STICK_DOWN
											myLeftInputAction = LEFT_STICK_LEFT
											myRightInputAction = LEFT_STICK_RIGHT
											myExpectedInputAction = LEFT_STICK_RIGHT
										}
										ConditionalInputAction "A Pressed After 200ms"
										{
											myInputActionType PressedAfterTime
											myTimer 200
											myInputAction = A_CROSS
										}
									}
								}
								ConditionalActionGroup KB
								{
									myConditionalInputActions
									{
										ConditionalRotatedInputAction "KB Hold Down Right"
										{
											myInputActionType HoldDown
											myForwardInputAction = KB_MOVE_UP
											myBackInputAction = KB_MOVE_DOWN
											myLeftInputAction = KB_MOVE_LEFT
											myRightInputAction = KB_MOVE_RIGHT
											myExpectedInputAction = KB_MOVE_RIGHT
										}
										ConditionalInputAction "KB Cover Button Pressed After 200ms"
										{
											myInputActionType PressedAfterTime
											myTimer 200
											myInputAction = KB_ACTION_1
										}
									}
								}
							}
						}
						ConditionalGameActionState "Option Set"
						{
							myGameActionState MoveAroundCornerUsingButtonPress
						}
					}
				}
			}
		}
	}
	myGameAction TakeCoverAroundCornerRight
}

TriggerGameAction "[Cover] Take Cover Around Left Corner"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Option Based Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalActionGroup Default
				{
					myConditionalInputActions
					{
						ConditionalActionGroup "Input Actions"
						{
							myOperator Or
							myConditionalInputActions
							{
								ConditionalRotatedInputAction "Rotated Left Stick Left Hold Down"
								{
									myInputActionType HoldDownAfterTime
									myTimer 400
									myForwardInputAction = LEFT_STICK_UP
									myBackInputAction = LEFT_STICK_DOWN
									myLeftInputAction = LEFT_STICK_LEFT
									myRightInputAction = LEFT_STICK_RIGHT
									myExpectedInputAction = LEFT_STICK_LEFT
								}
								ConditionalRotatedInputAction "KB Left Hold Down"
								{
									myInputActionType HoldDownAfterTime
									myTimer 400
									myForwardInputAction = KB_MOVE_UP
									myBackInputAction = KB_MOVE_DOWN
									myLeftInputAction = KB_MOVE_LEFT
									myRightInputAction = KB_MOVE_RIGHT
									myExpectedInputAction = KB_MOVE_LEFT
								}
							}
						}
						ConditionalGameActionState "In Cover"
						{
							myGameActionState InCover
						}
						ConditionalGameActionState "Is In Corner Cover"
						{
							myGameActionState IsCornerCoverAvailable
						}
						ConditionalGameActionState "Corner Timer Expired"
						{
							myGameActionState CornerTimerHasExpired
						}
						ConditionalGameActionState "Button Option Not Set"
						{
							myNegate TRUE
							myGameActionState MoveAroundCornerUsingButtonPress
						}
					}
				}
				ConditionalActionGroup ButtonPress
				{
					myConditionalInputActions
					{
						ConditionalGameActionState "In Cover"
						{
							myGameActionState InCover
						}
						ConditionalGameActionState "Is In Corner Cover"
						{
							myGameActionState IsCornerCoverAvailable
						}
						ConditionalActionGroup "Input Actions"
						{
							myOperator Or
							myConditionalInputActions
							{
								ConditionalActionGroup Gamepad
								{
									myConditionalInputActions
									{
										ConditionalInputAction "A Pressed After 200ms"
										{
											myInputActionType PressedAfterTime
											myTimer 200
											myInputAction = A_CROSS
										}
										ConditionalRotatedInputAction "Rotated Left Stick Left Hold Down"
										{
											myInputActionType HoldDown
											myForwardInputAction = LEFT_STICK_UP
											myBackInputAction = LEFT_STICK_DOWN
											myLeftInputAction = LEFT_STICK_LEFT
											myRightInputAction = LEFT_STICK_RIGHT
											myExpectedInputAction = LEFT_STICK_LEFT
										}
									}
								}
								ConditionalActionGroup KB
								{
									myConditionalInputActions
									{
										ConditionalInputAction "KB Cover Pressed After 200ms"
										{
											myInputActionType PressedAfterTime
											myTimer 200
											myInputAction = KB_ACTION_1
										}
										ConditionalRotatedInputAction "KB Move Left Hold Down"
										{
											myInputActionType HoldDown
											myForwardInputAction = KB_MOVE_UP
											myBackInputAction = KB_MOVE_DOWN
											myLeftInputAction = KB_MOVE_LEFT
											myRightInputAction = KB_MOVE_RIGHT
											myExpectedInputAction = KB_MOVE_LEFT
										}
									}
								}
							}
						}
						ConditionalGameActionState "Option Set"
						{
							myGameActionState MoveAroundCornerUsingButtonPress
						}
					}
				}
			}
		}
	}
	myGameAction TakeCoverAroundCornerLeft
}

TriggerGameAction "[Cover] Highlight Left Cover Corner"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalRotatedInputAction "Rotated Left Stick Left Hold Down"
				{
					myInputActionType HoldDown
					myForwardInputAction = LEFT_STICK_UP
					myBackInputAction = LEFT_STICK_DOWN
					myLeftInputAction = LEFT_STICK_LEFT
					myRightInputAction = LEFT_STICK_RIGHT
					myExpectedInputAction = LEFT_STICK_LEFT
				}
				ConditionalRotatedInputAction "KB Left Hold Down"
				{
					myInputActionType HoldDown
					myForwardInputAction = KB_MOVE_UP
					myBackInputAction = KB_MOVE_DOWN
					myLeftInputAction = KB_MOVE_LEFT
					myRightInputAction = KB_MOVE_RIGHT
					myExpectedInputAction = KB_MOVE_LEFT
				}
			}
		}
		ConditionalGameActionState "In Cover"
		{
			myGameActionState InCover
		}
		ConditionalGameActionState "Is In Corner Cover"
		{
			myGameActionState IsCornerCoverAvailable
		}
		ConditionalGameActionState "Not Aiming"
		{
			myNegate TRUE
		}
	}
	myGameAction IsCoverAroundCornerLeftAvailable
}

TriggerGameAction "[Cover] Highlight Horizontal Swap Left"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalRotatedInputAction "Rotated Left Stick Left Hold Down"
				{
					myInputActionType HoldDown
					myForwardInputAction = LEFT_STICK_UP
					myBackInputAction = LEFT_STICK_DOWN
					myLeftInputAction = LEFT_STICK_LEFT
					myRightInputAction = LEFT_STICK_RIGHT
					myExpectedInputAction = LEFT_STICK_LEFT
				}
				ConditionalRotatedInputAction "KB Left Hold Down"
				{
					myInputActionType HoldDown
					myForwardInputAction = KB_MOVE_UP
					myBackInputAction = KB_MOVE_DOWN
					myLeftInputAction = KB_MOVE_LEFT
					myRightInputAction = KB_MOVE_RIGHT
					myExpectedInputAction = KB_MOVE_LEFT
				}
			}
		}
		ConditionalGameActionState "In Cover"
		{
			myGameActionState InCover
		}
		ConditionalGameActionState "Is In Corner Cover"
		{
			myGameActionState IsCornerCoverAvailable
		}
		ConditionalGameActionState "Not Aiming"
		{
			myNegate TRUE
		}
	}
	myGameAction IsHorizontalSwapCoverLeftAvailable
}

