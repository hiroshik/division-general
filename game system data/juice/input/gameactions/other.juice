include ./movement.juice
include "rogue/game system data/juice/input/inputactions_kbui.juice"

TriggerGameAction "[Other] Interact"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "X-Square Held"
				{
					myInputActionType PressedAfterTime
					myTimer 200
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB Held"
				{
					myInputActionType PressedAfterTime
					myTimer 200
					myInputAction = KB_ACTION_3
				}
			}
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameActionState "Not Interacting"
		{
			myNegate TRUE
			myGameActionState Interacting
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
		ConditionalGameActionState CanInteract
		{
			myGameActionState CanInteract
		}
		ConditionalGameActionState "Not Bleeding Out"
		{
			myNegate TRUE
			myGameActionState IsBleedingOut
		}
		ConditionalGameActionState "Not Aiming"
		{
			myNegate TRUE
		}
	}
	myGameAction Interact
	myIsExclusiveToggle TRUE
}

TriggerGameAction "[Other] Progressive Interact"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "X-Square Held"
				{
					myInputActionType PressAndRelease
					myTimer 2000
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB Interact"
				{
					myInputActionType PressAndRelease
					myTimer 2000
					myInputAction = KB_ACTION_3
				}
			}
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
		ConditionalGameActionState CanInteract
		{
			myGameActionState CanInteract
		}
		ConditionalGameActionState "Not Bleeding Out"
		{
			myNegate TRUE
			myGameActionState IsBleedingOut
		}
	}
	myGameAction ProgressiveInteract
}

TriggerGameAction "[Other] Revive"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "X Square Held"
				{
					myInputActionType HoldDown
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB Interact"
				{
					myInputActionType HoldDown
					myInputAction = KB_ACTION_3
				}
			}
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
		ConditionalGameActionState "Not Bleeding Out"
		{
			myNegate TRUE
			myGameActionState IsBleedingOut
		}
		ConditionalGameActionState "Not Interacting With Extraction Rope"
		{
			myNegate TRUE
			myGameActionState IsInteractingWithExtractionRope
		}
		ConditionalGameActionState "Not Starting Extraction"
		{
			myNegate TRUE
			myGameActionState IsStartingExtraction
		}
		ConditionalGameActionState "Not Staggered"
		{
			myNegate TRUE
			myGameActionState IsStaggered
		}
		ConditionalGameActionState "Not Rolling"
		{
			myNegate TRUE
			myGameActionState Rolling
		}
	}
	myGameAction Revive
}

TriggerGameAction "[Other] Skill"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "Y Triangle Held"
				{
					myInputActionType HoldDown
					myInputAction = Y_TRIANGLE
				}
			}
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameAction "Not Moving"
		{
			myGameAction NotMoving
		}
	}
	myGameAction Skill
}

TriggerGameAction "[Other] Support Station Revive Start"
{
	myConditionalInputActions
	{
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameActionState "Is Downed"
		{
			myGameActionState IsDowned
		}
	}
	myGameAction SupportStationReviveStart
}

TriggerGameAction "[Other] Support Station Revive Update"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "X Square Held"
				{
					myInputActionType HoldDown
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB Interact"
				{
					myInputActionType HoldDown
					myInputAction = KB_ACTION_3
				}
			}
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
	}
	myGameAction SupportStationReviveUpdate
}

TriggerGameAction "[Other] Support Station Ammo Resupply"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "X Square Held"
				{
					myInputActionType HoldDown
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB Interact"
				{
					myInputActionType HoldDown
					myInputAction = KB_ACTION_3
				}
			}
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameAction "Not Moving"
		{
			myGameAction NotMoving
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
	}
	myGameAction SupportStationAmmoResupply
}

TriggerGameAction "[Other] Support Station Ammo Resupply Start"
{
	myConditionalInputActions
	{
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameAction "Not Moving"
		{
			myGameAction NotMoving
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
	}
	myGameAction SupportStationAmmoResupplyStart
}

TriggerGameAction "[Other] Loot"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "D-PAD up"
				{
					myInputActionType Pressed
					myInputAction = D_PAD_UP
				}
				ConditionalInputAction "D-PAD up"
				{
					myInputActionType PressAndReleaseBeforeTime
					myTimer 300
					myInputAction = KB_ACTION_3
				}
			}
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameActionState "Not Interacting"
		{
			myNegate TRUE
			myGameActionState Interacting
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
		ConditionalGameActionState "Can Loot"
		{
			myGameActionState CanLoot
		}
		ConditionalGameActionState "Not In Weapon Switch"
		{
			myNegate TRUE
			myGameActionState IsInWeaponSwitch
		}
		ConditionalGameActionState "Can Interact"
		{
			myGameActionState CanInteract
		}
		ConditionalGameActionState "Not in Cover To Cover"
		{
			myNegate TRUE
			myGameActionState InCoverToCover
		}
	}
	myGameAction Loot
	myIsExclusiveToggle TRUE
}

TriggerGameAction "[Other] Loot All"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "X Square Press and hold"
				{
					myInputActionType PressedAfterTime
					myTimer 300
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB Interact"
				{
					myInputActionType PressedAfterTime
					myTimer 300
					myInputAction = KB_ACTION_3
				}
			}
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameActionState "Not Interacting"
		{
			myNegate TRUE
			myGameActionState Interacting
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
		ConditionalGameActionState "Can Loot"
		{
			myGameActionState CanLoot
		}
		ConditionalGameActionState "Not in Weapon Switch"
		{
			myNegate TRUE
			myGameActionState IsInWeaponSwitch
		}
		ConditionalGameActionState "Can Interact"
		{
			myGameActionState CanInteract
		}
	}
	myGameAction LootAll
}

TriggerGameAction "[Other] Use Navigation"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "B Circle Press"
				{
					myInputActionType Pressed
					myInputAction = B_CRICLE
				}
				ConditionalActionGroup "KB Inputs"
				{
					myOperator Or
					myConditionalInputActions
					{
						ConditionalActionGroup "KB Not in Cover"
						{
							myOperator And
							myConditionalInputActions
							{
								ConditionalInputAction "KB Press"
								{
									myInputActionType Pressed
									myInputAction = KB_ACTION_2
								}
								ConditionalGameActionState "Not In Cover"
								{
									myNegate TRUE
									myGameActionState InCover
								}
							}
						}
						ConditionalActionGroup "KB in Cover"
						{
							myOperator And
							myConditionalInputActions
							{
								ConditionalInputAction "KB Press"
								{
									myInputActionType SinglePress
									myTimer 300
									myInputAction = KB_ACTION_2
								}
								ConditionalGameActionState "Not In Cover"
								{
									myGameActionState InCover
								}
							}
						}
					}
				}
			}
		}
		ConditionalGameActionState "Not Interacting"
		{
			myNegate TRUE
			myGameActionState Interacting
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
		ConditionalGameActionState "Can Use Navigation"
		{
			myGameActionState CanUseNavigation
		}
		ConditionalGameActionState "Not In Slide To Cover"
		{
			myNegate TRUE
			myGameActionState Sliding
		}
		ConditionalGameActionState "Not Interacting With Extraction Rope"
		{
			myNegate TRUE
			myGameActionState IsInteractingWithExtractionRope
		}
		ConditionalGameActionState "Not Starting Extraction"
		{
			myNegate TRUE
			myGameActionState IsStartingExtraction
		}
		ConditionalGameActionState "Not Aiming Skill/Grenade"
		{
			myNegate TRUE
			myGameActionState IsAimingSkill
		}
	}
	myGameAction UseNavigation
}

TriggerGameAction "[Other] Held Navigation"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "B Circle Held"
				{
					myInputActionType HoldDown
					myTimer 0
					myInputAction = B_CRICLE
				}
				ConditionalActionGroup "KB Input"
				{
					myOperator And
					myConditionalInputActions
					{
						ConditionalInputAction "KB Held"
						{
							myInputActionType HoldDown
							myTimer 300
							myInputAction = KB_ACTION_2
						}
						ConditionalGameActionState "Not In Cover"
						{
							myNegate TRUE
							myGameActionState InCover
						}
					}
				}
				ConditionalActionGroup "Parkour Mode"
				{
					myOperator And
					myConditionalInputActions
					{
						ConditionalGameActionState "Parkour Enabled"
						{
							myGameActionState IsInParkourMode
						}
						ConditionalGameActionState "Is Sprinting"
						{
							myGameActionState Sprinting
						}
					}
				}
			}
		}
		ConditionalGameActionState "Not Interacting"
		{
			myNegate TRUE
			myGameActionState Interacting
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
		ConditionalGameActionState "Can Use Navigation"
		{
			myGameActionState CanUseNavigation
		}
		ConditionalGameActionState "Not In Slide To Cover"
		{
			myNegate TRUE
			myGameActionState Sliding
		}
		ConditionalGameActionState "Not Interacting With Extraction Rope"
		{
			myNegate TRUE
			myGameActionState IsInteractingWithExtractionRope
		}
		ConditionalGameActionState "Not Starting Extraction"
		{
			myNegate TRUE
			myGameActionState IsStartingExtraction
		}
		ConditionalGameActionState "Not Cancel Aiming Skill Cooldown Time Active"
		{
			myNegate TRUE
			myGameActionState IsCancelAimingSkillCooldownTimeActive
		}
		ConditionalGameActionState "Not Aiming Skill/Grenade"
		{
			myNegate TRUE
			myGameActionState IsAimingSkill
		}
	}
	myGameAction HeldNavigation
}

TriggerGameAction "[Other] Cover Bonnet Vault"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "A Cross Release"
				{
					myInputActionType PressAndReleaseBeforeTime
					myTimer 300
					myInputAction = A_CROSS
				}
				ConditionalInputAction "KB Pressed"
				{
					myInputActionType PressAndReleaseBeforeTime
					myTimer 300
					myInputAction = KB_ACTION_1
				}
			}
		}
		ConditionalGameActionState "Not Interacting"
		{
			myNegate TRUE
			myGameActionState Interacting
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
		ConditionalGameActionState "Can Use Navigation"
		{
			myGameActionState CanUseNavigation
		}
		ConditionalGameActionState "In Cover"
		{
			myNegate FALSE
			myGameActionState InCover
		}
		ConditionalGameActionState "Not Aiming Skill"
		{
			myNegate TRUE
			myGameActionState IsAimingSkill
		}
		ConditionalGameActionState "Not In Slide To Cover"
		{
			myNegate TRUE
			myGameActionState Sliding
		}
		ConditionalGameActionState "Not Interacting With Extraction Rope"
		{
			myNegate TRUE
			myGameActionState IsInteractingWithExtractionRope
		}
		ConditionalGameActionState "Not Starting Extraction"
		{
			myNegate TRUE
			myGameActionState IsStartingExtraction
		}
		ConditionalGameActionState "Not in Safe Zone"
		{
			myNegate TRUE
			myGameActionState IsInSafeZone
		}
	}
	myGameAction CoverBonnetVault
}

TriggerGameAction "[Other] Detach Navigation"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "B Circle Press"
				{
					myInputActionType Pressed
					myInputAction = B_CRICLE
				}
				ConditionalInputAction "KB Press"
				{
					myInputActionType Pressed
					myInputAction = KB_ACTION_2
				}
			}
		}
		ConditionalGameActionState "Using Navigation"
		{
			myGameActionState UsingNavigation
		}
	}
	myGameAction DetachNavigation
}

TriggerGameAction "[Other] Downed and Moving"
{
	myConditionalInputActions
	{
		ConditionalGameActionState Downed
		{
			myGameActionState IsDowned
		}
		ConditionalGameAction "Not Moving"
		{
			myNegate TRUE
			myGameAction NotMoving
		}
	}
	myGameAction DownAndMoving
}

TriggerGameAction "[Other] Die Quicker"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "B Circle Hold Down"
				{
					myInputActionType HoldDown
					myInputAction = B_CRICLE
				}
				ConditionalInputAction "KB Hold"
				{
					myInputActionType HoldDown
					myInputAction = KB_ACTION_1
				}
			}
		}
		ConditionalGameActionState "Is Dying"
		{
			myGameActionState IsDying
		}
	}
	myGameAction DieQuicker
}

TriggerGameAction "[Other] Loot All UI Release"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "Y Release"
				{
					myInputActionType Released
					myInputAction = Y_TRIANGLE
				}
			}
		}
	}
	myGameAction LootAll
	myIsUIAction TRUE
}

TriggerGameAction "[Other] Loot All UI Hold"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "Y Hold"
				{
					myInputActionType PressedAfterTime
					myTimer 300
					myInputAction = Y_TRIANGLE
				}
				ConditionalInputAction "KB Hold"
				{
					myInputActionType PressedAfterTime
					myTimer 300
					myInputAction = KB_ACTION_3
				}
			}
		}
	}
	myGameAction LootAll
	myIsUIAction TRUE
}

TriggerGameAction "[Other] Pick Up"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "X-Square PressedAfterTime"
				{
					myInputActionType PressedAfterTime
					myTimer 200
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB PressedAfterTime"
				{
					myInputActionType PressedAfterTime
					myTimer 200
					myInputAction = KB_ACTION_3
				}
			}
		}
		ConditionalGameActionState "Is Not Carrying"
		{
			myNegate TRUE
			myGameActionState IsCarrying
		}
		ConditionalGameActionState "Can Pick Up"
		{
			myGameActionState CanPickUp
		}
		ConditionalGameActionState "Not Aiming"
		{
			myNegate TRUE
		}
	}
	myGameAction PickUp
}

TriggerGameAction "[Other] Drop Off"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "X-Square PressedAfterTime"
				{
					myInputActionType PressedAfterTime
					myTimer 200
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB PressedAfterTime"
				{
					myInputActionType PressedAfterTime
					myTimer 200
					myInputAction = KB_ACTION_3
				}
			}
		}
		ConditionalGameActionState "Is Carrying"
		{
			myGameActionState IsCarrying
		}
		ConditionalGameActionState "Can Drop Off"
		{
			myGameActionState CanDropOff
		}
		ConditionalGameActionState "Not Aiming"
		{
			myNegate TRUE
		}
	}
	myGameAction DropOff
}

TriggerGameAction "[Other] Drop"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalActionGroup "GamePad Buttons"
				{
					myOperator Or
					myConditionalInputActions
					{
						ConditionalInputAction "A Cross Pressed"
						{
							myInputActionType Pressed
							myInputAction = A_CROSS
						}
						ConditionalInputAction "Y Triangle Pressed"
						{
							myInputActionType Pressed
							myInputAction = Y_TRIANGLE
						}
						ConditionalActionGroup "B Circle Pressed"
						{
							myConditionalInputActions
							{
								ConditionalInputAction "B Circle Pressed"
								{
									myInputActionType Pressed
									myInputAction = B_CRICLE
								}
							}
						}
					}
				}
				ConditionalActionGroup "Keyboard Buttons"
				{
					myOperator Or
					myConditionalInputActions
					{
						ConditionalInputAction "KB Action 1 Pressed"
						{
							myInputAction = KB_ACTION_1
						}
						ConditionalInputAction "KB Action 2 Pressed"
						{
							myInputActionType Pressed
							myInputAction = KB_ACTION_2
						}
						ConditionalInputAction "KB Select Primary Pressed"
						{
							myInputAction = KB_SELECT_PRIMARY
						}
						ConditionalInputAction "KB Select Secondary Pressed"
						{
							myInputAction = KB_SELECT_SECONDARY
						}
						ConditionalInputAction "KB Cycle Weapon Up"
						{
							myInputAction = M_WHEEL_UP
						}
						ConditionalInputAction "KB Cycle Weapon Down"
						{
							myInputAction = M_WHEEL_DOWN
						}
					}
				}
				ConditionalActionGroup Inventory
				{
					myOperator Or
					myConditionalInputActions
					{
						ConditionalInputAction "Start Pressed"
						{
							myInputAction = START
						}
						ConditionalInputAction "KB Inventory"
						{
							myInputAction = KB_INVENTORY_MENU
						}
						ConditionalInputAction "KB Social"
						{
							myInputAction = KB_SOCIAL_MENU
						}
						ConditionalInputAction "KB Skills"
						{
							myInputAction = KB_SKILLS_MENU
						}
						ConditionalInputAction "KB Perks"
						{
							myInputAction = KB_PERKS_MENU
						}
						ConditionalInputAction "KB Talents"
						{
							myInputAction = KB_TALENTS_MENU
						}
						ConditionalInputAction "KB Escape"
						{
							myInputAction = ESCAPE
						}
					}
				}
			}
		}
		ConditionalGameActionState "Is Carrying"
		{
			myGameActionState IsCarrying
		}
	}
	myGameAction Drop
}

TriggerGameAction "[Other] Melee button mashing"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction B_CIRCLE
				{
					myInputAction = B_CRICLE
				}
				ConditionalInputAction "KB Press"
				{
					myInputAction = KB_ACTION_1
				}
			}
		}
	}
	myGameAction MeleeButtonMashing
	myIsUIAction FALSE
}

TriggerGameAction "[Other] Melee button mashing Mouse"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "Mouse Trigger Pressed"
				{
					myInputAction = RIGHT_TRIGGER_R2
				}
				ConditionalInputAction "Mouse Trigger Pressed"
				{
					myInputAction = KB_SHOOT
				}
			}
		}
	}
	myGameAction MeleeButtonMashing
	myIsUIAction FALSE
}

TriggerGameAction "[Other] Cancel Interact Path"
{
	myConditionalInputActions
	{
		ConditionalGameActionState "Is On Path"
		{
			myGameActionState IsOnPath
		}
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "X-Square released"
				{
					myInputActionType Released
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB released"
				{
					myInputActionType Released
					myInputAction = KB_ACTION_3
				}
			}
		}
		ConditionalGameActionState "Not Interacting With Extraction Rope"
		{
			myNegate TRUE
			myGameActionState IsInteractingWithExtractionRope
		}
	}
	myGameAction CancelPath
}

TriggerGameAction "[Other] Extract Items Start"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "Hold X"
				{
					myInputActionType Pressed
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB Hold"
				{
					myInputActionType Pressed
					myInputAction = KB_ACTION_3
				}
			}
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameActionState "Not Interacting"
		{
			myNegate TRUE
			myGameActionState Interacting
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
		ConditionalGameActionState "Can Interact"
		{
			myGameActionState CanInteract
		}
		ConditionalGameActionState "Can Interact With Extraction Rope"
		{
			myGameActionState CanInteractWithExtractionRope
		}
	}
	myGameAction ExtractItemsStart
}

TriggerGameAction "[Other] Extract Items Cancel"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "Release X"
				{
					myInputActionType Released
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB Interact"
				{
					myInputActionType Released
					myInputAction = KB_ACTION_3
				}
			}
		}
	}
	myGameAction ExtractItemsCancel
}

TriggerGameAction "[Other] Queue Use Navigation"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "B Circle Press"
				{
					myInputAction = B_CRICLE
				}
				ConditionalInputAction "KB Press"
				{
					myInputAction = KB_ACTION_2
				}
			}
		}
		ConditionalGameActionState "Not Interacting"
		{
			myNegate TRUE
			myGameActionState Interacting
		}
		ConditionalGameActionState "Not Using Navigation"
		{
			myNegate TRUE
			myGameActionState UsingNavigation
		}
		ConditionalGameActionState "Not Downed"
		{
			myNegate TRUE
			myGameActionState IsDowned
		}
		ConditionalGameActionState "Not Carrying"
		{
			myNegate TRUE
			myGameActionState IsCarrying
		}
		ConditionalGameActionState "Not In Cover"
		{
			myNegate TRUE
			myGameActionState InCover
		}
		ConditionalGameActionState "Not Aiming"
		{
			myNegate TRUE
		}
		ConditionalGameActionState "Not In Cover to Cover"
		{
			myNegate TRUE
			myGameActionState InCoverToCover
		}
		ConditionalGameActionState "Not Rolling"
		{
			myNegate TRUE
			myGameActionState Rolling
		}
		ConditionalGameActionState "Not Aiming Skill"
		{
			myNegate TRUE
			myGameActionState IsAimingSkill
		}
		ConditionalGameActionState "Not Interacting With Extraction Rope"
		{
			myNegate TRUE
			myGameActionState IsInteractingWithExtractionRope
		}
		ConditionalGameActionState "Not Starting Extraction"
		{
			myNegate TRUE
			myGameActionState IsStartingExtraction
		}
		ConditionalGameActionState "Not in Safe Zone"
		{
			myNegate TRUE
			myGameActionState IsInSafeZone
		}
	}
	myGameAction QueueUseNavigation
}

TriggerGameAction "[Other] Cancel Interaction"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "B-Circle Pressed"
				{
					myInputActionType Pressed
					myInputAction = B_CRICLE
				}
				ConditionalInputAction keyboard_press
				{
					myInputActionType Pressed
					myInputAction = KB_ACTION_2
				}
			}
		}
		ConditionalGameActionState "Is interacting"
		{
			myGameActionState Interacting
		}
	}
	myGameAction CancelInteraction
}

TriggerGameAction "[Other] Cancel InteractionOutroAnim"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "B-Circle Pressed"
				{
					myInputActionType Pressed
					myInputAction = B_CRICLE
				}
				ConditionalInputAction keyboard_press
				{
					myInputActionType Pressed
					myInputAction = KB_ACTION_2
				}
				ConditionalInputAction "X-square pressed"
				{
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "Y-triangle pressed"
				{
					myInputAction = Y_TRIANGLE
				}
				ConditionalInputAction "A-cross pressed"
				{
					myInputAction = A_CROSS
				}
				ConditionalInputAction "RT-R2 pressed"
				{
					myInputAction = RIGHT_TRIGGER_R2
				}
				ConditionalInputAction "LT-L2 pressed"
				{
					myInputAction = LEFT_TRIGGER_L2
				}
				ConditionalInputAction "LB-L1 pressed"
				{
					myInputAction = LEFT_BUMPER_L1
				}
				ConditionalInputAction "RB-R1 pressed"
				{
					myInputAction = RIGHT_BUMPER_R1
				}
			}
		}
		ConditionalGameActionState "Is interacting"
		{
			myGameActionState Interacting
		}
	}
	myGameAction CancelInteractionOutroAnim
}

TriggerGameAction "[Other] Look at Teammate 1"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction keyboard_press
				{
					myInputAction = KB_TEAMMATE_1
				}
			}
		}
	}
	myGameAction LookAtTeammate1
}

TriggerGameAction "[Other] Look at Teammate 2"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction keyboard_press
				{
					myInputAction = KB_TEAMMATE_2
				}
			}
		}
	}
	myGameAction LookAtTeammate2
}

TriggerGameAction "[Other] Look at Teammate 3"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction keyboard_press
				{
					myInputAction = KB_TEAMMATE_3
				}
			}
		}
	}
	myGameAction LookAtTeammate3
}

TriggerGameAction "[Other] HoldInteractionIsHeld"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "X Hold Down After Time"
				{
					myInputActionType HoldDownAfterTime
					myTimer 200
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB Hold Down After Time"
				{
					myInputActionType HoldDownAfterTime
					myTimer 200
					myInputAction = KB_ACTION_3
				}
			}
		}
	}
	
	ConditionalGameActionState "Is Interacting"
	{
		myGameActionState Interacting
	}

	myGameAction HoldInteractionIsHeld
}

TriggerGameAction "[Other] AbortExtractionInteractionContext"
{
	myConditionalInputActions
	{
		ConditionalActionGroup "Input Actions"
		{
			myOperator Or
			myConditionalInputActions
			{
				ConditionalInputAction "X-Square Pressed"
				{
					myInputActionType Pressed
					myInputAction = X_SQUARE
				}
				ConditionalInputAction "KB Pressed"
				{
					myInputActionType Pressed
					myInputAction = KB_ACTION_3
				}
			}
		}
	}
	
	ConditionalGameActionState "Is Interacting With Extraction Rope"
	{
		myGameActionState IsInteractingWithExtractionRope
	}

	myGameAction AbortExtractionInteractionContext
}